# OpenAI Test

## Setting up env

Install miniconda - MacOSX 64-bit pkg
https://docs.conda.io/en/latest/miniconda.html

*You might have to restart your terminal for the conda path to be recognized*

Create a new python environment for the project
`conda create --name openai`

Activate new env
`conda activate openai`

Install jupyter
`conda install jupyter`

Install openai package
`conda install -c conda-forge openai`

Start server
`jupyter notebook`

You should see jupyter notebook launch in the browser

### src/config.py
These should be the content

`OPENAI_KEY = 'sk.....'`

### Everytime you start a terminal
Run `conda activate openai`, to activate the environment

Start server
`jupyter notebook`

## Version control
Not sure what's the best practice is, but we can try to follow the JS workflow and 
commit jupyter notebooks like any other codes.  If we work on the same notebooks
and made conflicting changes, I don't think we can reliable resolve conflicts since
it's not really python code.  

## Update dependencies

